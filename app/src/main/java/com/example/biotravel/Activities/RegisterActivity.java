package com.example.biotravel.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.biotravel.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends AppCompatActivity {

    CircleImageView ImgUserPhoto;
    static int PreqCode = 1;
    static int REQUESTCODE = 1;
    Uri pilihImgUri;

    private EditText userName,userEmail,userPassword,userPassword2;
    private ProgressBar loadingProgress;
    private Button btnReg;

    private FirebaseAuth mAuth;

    TextView pindah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        //inisialisasi view
        ImgUserPhoto = findViewById(R.id.img_reg_profil_register);
        userName = findViewById(R.id.et_reg_nama_lengkap);
        userEmail = findViewById(R.id.et_reg_email);
        userPassword = findViewById(R.id.et_reg_password);
        userPassword2 = findViewById(R.id.et_reg_password2);
        loadingProgress = findViewById(R.id.regProgressBar);
        btnReg = findViewById(R.id.btn_reg_daftar);

        loadingProgress.setVisibility(View.INVISIBLE);

        pindah = findViewById(R.id.txt_login);
        pindah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(a);
                finish();
            }
        });

        mAuth = FirebaseAuth.getInstance();

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnReg.setVisibility(View.INVISIBLE);
                loadingProgress.setVisibility(View.VISIBLE);
                final String email = userEmail.getText().toString();
                final String password = userPassword.getText().toString();
                final String password2 = userPassword2.getText().toString();
                final String name = userName.getText().toString();

                if (email.isEmpty() || password.isEmpty() || name.isEmpty() || !password.equals(password2))
                {
                    //menampilkan kesalahan pengisian data registrasi
                    showMassage("Tolong isi data dengan lengkap");
                    btnReg.setVisibility(View.VISIBLE);
                    loadingProgress.setVisibility(View.INVISIBLE);


                } else
                {
                    //untuk membuat akun baru

                    CreateUserAccount(email,password,name);
                }

            }
        });

        ImgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 22){
                    checkAndRequestForPermission();

                }
                else {
                    openGaleri();
                }
            }
        });

    }

    private void CreateUserAccount(String email, String password, final String name) {
        //method untuk membuat akun dengan email dan password

        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful())
                {
                    //user akun sukses dibuat
                    showMassage("Berhasil Terdaftar");
                    //ketika akun sudah dibuat, kita butuh gambar profil untuk update dan nama
                    updateUserInfo(name,pilihImgUri,mAuth.getCurrentUser());

                } else
                {
                    //akun gagal
                    showMassage("Akun gagal dibuat" + task.getException().getMessage());
                    btnReg.setVisibility(View.VISIBLE);
                    loadingProgress.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    private void updateUserInfo(final String name, Uri pilihImgUri, final FirebaseUser currentUser) {
        //method untuk update foto user dan nama
        //kita membutuhkan storage firebase
        StorageReference mStorage = FirebaseStorage.getInstance().getReference().child("users_photo");
        final StorageReference imageFilePath = mStorage.child(pilihImgUri.getLastPathSegment());
        imageFilePath.putFile(pilihImgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                //gambar berhasil di upload
                //kita membutuhkan image url
                imageFilePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        //uri contain user image url

                        UserProfileChangeRequest profilUpdate = new UserProfileChangeRequest.Builder().setDisplayName(name).setPhotoUri(uri).build();

                        currentUser.updateProfile(profilUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                if (task.isSuccessful())
                                {
                                    // pemberitahuan sukses update
                                    showMassage("Regiter Complete");
                                    updateUI();
                                }
                            }
                        });

                    }
                });
            }
        });

    }

    private void updateUI() {
        Intent homeActivity = new Intent(getApplicationContext(),Home.class);
        startActivity(homeActivity);
        finish();
    }

    private void showMassage(String pesan) {
        //method untuk menampilkan toast
        Toast.makeText(getApplicationContext(),pesan,Toast.LENGTH_SHORT).show();
    }

    private void openGaleri() {
        //Method User membuka galeri

        Intent galeriIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galeriIntent.setType("image/*");
        startActivityForResult(galeriIntent,REQUESTCODE);

    }

    private void checkAndRequestForPermission() {
        if (ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE))
            {

                Toast.makeText(RegisterActivity.this, "Please accept for required permission", Toast.LENGTH_SHORT).show();
            }
            else
            {
                ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PreqCode);
            }
        } else
        {
            openGaleri();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUESTCODE && data != null )
        {
            //user bisa memilih gambar profil
            //membutuhkan penyimpanan gambar Uri

            pilihImgUri = data.getData();
            ImgUserPhoto.setImageURI(pilihImgUri);

        }
    }
}
